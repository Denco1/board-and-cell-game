import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faCircle } from "@fortawesome/free-solid-svg-icons";


// CellProps: It is handing Cell Functional Component props, which shows cell of board.

type CellProps = {
  value: number;
  playCell?: () => void;
  win: boolean;
};

const Cell = ({ value, playCell, win }: CellProps) => {
  return (
    <div className={`Board-cell${value ? " played" : ""}`} onClick={playCell} style={win == true ? { backgroundColor: "#a3072b", padding: 10 } : { backgroundColor: "#5a03fc", padding: 10 }}>
      {!!value && <FontAwesomeIcon icon={value === 1 ? faTimes : faCircle} style={{ fontSize: 90, textAlign: "center", marginLeft: 10 }} />}
    </div>
  );
};

export default Cell;
