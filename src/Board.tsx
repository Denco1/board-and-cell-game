import React from "react";
import Cell from "./Cell";
import { Result } from "./Types";


// BoardProps: It is handling props (properties) of board component. 

type BoardProps = {
  values: number[][];
  playCell: (row: number, cell: number) => void;
};

const Board = ({ values, playCell }: BoardProps) => {
  var tempValues = checkWin(values);
  return (
    <div className="Board">
      {values.map((rowValues, row) => (
        <div key={row} className="Board-row" >
          {rowValues.map((rowValue, col) => (
            <Cell
              win={tempValues[row][col] == 1 ? true : false}
              key={col}
              value={rowValue}
              playCell={() => playCell(row, col)}
            />

          ))}
        </div>
      ))}
    </div>
  );
};


const checkWin = (values: number[][]) => {
  //for row checking  
  if (values[0][0] == values[0][1] && values[0][0] == values[0][2] && values[0][0] != 0) {
    return [[1, 1, 1], [0, 0, 0], [0, 0, 0]]

  }

  else if (values[1][0] == values[1][1] && values[1][0] == values[1][2] && values[1][0] != 0) {
    return [[0, 0, 0], [1, 1, 1], [0, 0, 0]]

  }

  else if (values[2][0] == values[2][1] && values[2][0] == values[2][2] && values[2][0] != 0) {
    return [[0, 0, 0], [0, 0, 0], [1, 1, 1]]
  }

  //for col checking 
  else if (values[0][0] == values[1][0] && values[0][0] == values[2][0] && values[0][0] != 0) {
    return [[1, 0, 0], [1, 0, 0], [1, 0, 0]]
  }

  else if (values[0][1] == values[1][1] && values[0][1] == values[2][1] && values[0][1] != 0) {
    return [[0, 1, 0], [0, 1, 0], [0, 1, 0]]
  }

  else if (values[0][2] == values[1][2] && values[0][2] == values[2][2] && values[0][2] != 0) {
    return [[0, 0, 1], [0, 0, 1], [0, 0, 1]]
  }

  //for diagonal checking 
  else if (values[0][0] == values[1][1] && values[0][0] == values[2][2] && values[0][0] != 0) {
    return [[1, 0, 0], [0, 1, 0], [0, 0, 1]]

  }

  else if (values[0][2] == values[1][1] && values[0][2] == values[2][0] && values[2][0] != 0) {
    return [[0, 0, 1], [0, 1, 0], [1, 0, 0]]
  }

  //for draw checking 
  else {
    var count = 0;
    for (var i = 0; i < 3; i++)
      for (var j = 0; j < 3; j++)
        if (values[i][j] != 0)
          count++;

    if (count == 9) {
      return [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    }

  }
  return [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

}




export default Board;
