import React, { useState, useReducer } from "react";
import "./App.scss";

import Board from "./Board";
import FeedbackBar from "./FeedbackBar";
import { Result } from "./Types";
import Popup from "reactjs-popup";


export const AppContext = React.createContext;

const App = () => {
  // Values for the board cells
  // 0 = Not played
  // 1 = played by player X
  // -1 = played by player O

  // Values 2D matrics for handling dashboeard
  const [values, setValues] = useState([
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0]
  ]);

  //Player varibale for handling players
  //1 for player1 and -1 for player2
  const [player, setPlayer] = useState(1);

  //Reset flag for reasting game
  const [restF, setrestF] = useState(0);

  //Complete flag for handling completion of game
  const [completeF, setcompleteF] = useState(0);




  //playCell : This method handle each click of dashboard and set value according to player's turn
  const playCell = (row: number, col: number) => {
    const result: Result = getResult(values);
    if (result.draw == false && result.winPlayer == 0) {
      values[row][col] = player;
      setValues(values);
      setPlayer(-player);
      getResult(values)
    }

  };

  //restGame : This method use for reset game.
  const resetGame = () => {
    handleDashboard(0)
  };


  //handleDashboard: This method will manage all dashboard and handle variable like completeF, Value, restF
  const handleDashboard = (status: number) => {
    var val: any = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
    var val2: any = values
    var res: any = status == 0 ? val : val2

    setcompleteF(status);
    setValues(res);
    setPlayer(player);
    setrestF(1);
  }




  //getResult: This method will provid result of game. like cheking the winning status and draw status of game
  const getResult = (values: number[][]): Result => {

    //for row checking  
    if (values[0][0] == values[0][1] && values[0][0] == values[0][2] && values[0][0] != 0) {
      handleDashboard(1)
      return {
        winPlayer: player
      };

    }

    else if (values[1][0] == values[1][1] && values[1][0] == values[1][2] && values[1][0] != 0) {
      handleDashboard(1)
      return {
        winPlayer: player
      };

    }

    else if (values[2][0] == values[2][1] && values[2][0] == values[2][2] && values[2][0] != 0) {
      handleDashboard(1)
      return {
        winPlayer: player
      };

    }

    //for col checking 
    else if (values[0][0] == values[1][0] && values[0][0] == values[2][0] && values[0][0] != 0) {
      handleDashboard(1)
      return {
        winPlayer: player
      };

    }

    else if (values[0][1] == values[1][1] && values[0][1] == values[2][1] && values[0][1] != 0) {
      handleDashboard(1)
      return {
        winPlayer: player
      };

    }

    else if (values[0][2] == values[1][2] && values[0][2] == values[2][2] && values[0][2] != 0) {
      handleDashboard(1)
      return {
        winPlayer: player
      };

    }

    //for diagonal checking 
    else if (values[0][0] == values[1][1] && values[0][0] == values[2][2] && values[0][0] != 0) {
      handleDashboard(1)
      return {
        winPlayer: player
      };

    }

    else if (values[0][2] == values[1][1] && values[0][2] == values[2][0] && values[2][0] != 0) {
      handleDashboard(1)
      return {
        winPlayer: player
      };

    }

    //for draw checking 
    else {
      var count = 0;
      for (var i = 0; i < 3; i++)
        for (var j = 0; j < 3; j++)
          if (values[i][j] != 0)
            count++;

      if (count == 9) {
        handleDashboard(2)
        return {
          draw: true
        };
      }

    }

    return {
      winPlayer: 0,
      draw: false
    }

  };


// dashboard : It is view-widget variable which shows board also show feedback message if game complete i.e win/draw

  var dashboard = (
    <div className="App">
      <div className="App-center">
        <FeedbackBar {...{ player, resetGame }} />
        <Board {...{ values, playCell, getResult }} />
      </div>
      {
        completeF != 0 &&
        <div className="App-center" style={{ backgroundColor: "white", position: "absolute", width: 400, height: 250, fontSize: 36, color: "black", textAlign: "center", padding: 10, borderWidth: 3, borderColor: 'red', borderRadius: 10 }}>
          <p>Game Over</p>
          {
            completeF == 1 ?
              <p>Player {
                player == 1 ? "X" : "O"
              } won</p>
              :
              <p>Its Draw</p>
          }
          <button className="FeedbackBar-button" onClick={() => { setcompleteF(0); }} style={{ backgroundColor: "#039dfc", borderWidth: 1, width: 150, height: 50, borderRadius: 10, color: "white", fontSize: 20, outline: "none" }}> OK </button>
        </div>
      }

    </div>
  )
  if (restF == 0)
    return dashboard

  else {
    setrestF(0);
    return dashboard
  }

};

export default App;
