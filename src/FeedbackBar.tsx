import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faCircle } from "@fortawesome/free-solid-svg-icons";
import { Result } from "./Types";

// FeedbackBarProps: It is handling Feedbar Funtional Component props, that shows user-turn and rest button. 

type FeedbackBarProps = {
  player: number;
  resetGame: () => void;
};

const FeedbackBar = ({ player, resetGame }: FeedbackBarProps) => {
  return (
    <div className="FeedbackBar" style={{ backgroundColor: "#4257bd" }}>
      <div className="FeedbackBar-message" style={{ height: 50, textAlign: "center", paddingTop: 20, }}>
        <span style={{ fontSize: 20 }}>
          Au joueur :
          <FontAwesomeIcon icon={player === 1 ? faTimes : faCircle} style={{ fontSize: 20, marginLeft: 10, marginBottom: -2, }} />
          <span style={{ marginLeft: 10 }}>
            de jouer.
          </span>
        </span>
      </div>
      <div style={{
        margin: "auto", textAlign: "center", padding: 10
      }}>
        <button className="FeedbackBar-button" onClick={resetGame} style={{ backgroundColor: "#039dfc", borderWidth: 1, width: 150, height: 50, borderRadius: 10, color: "white", fontSize: 20 }}>
          Réinitialiser
        </button>
      </div>
    </div>
  );
};

export default FeedbackBar;
